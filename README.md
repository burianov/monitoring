# Simple monitoring stack
This stack include:
* opensearch 2.11.0 - 2 nodes
* opensearch dashboad 2.11.0
* logstash with GeoIP database
* victoriametrics for store prometheus metrics - 2 nodes
* victoriametrics auth
* opentelemetry collector
* jaeger with store spins to opensearch
* zipkin
* nginx

## Requirements:
* CPU: 2 core
* RAM: 8GB
* Disk: 5GB images + storage for opensearch and victoriametrics

## Run
```bash
docker compose up --remove-orphans --pull always --build -d
```
### generate opensearch password
```bash
dd if=/dev/urandom count=1 bs=16 | base64
/usr/share/opensearch/plugins/opensearch-security/tools/hash.sh
/usr/share/opensearch/plugins/opensearch-security/tools/securityadmin.sh \
  -cd /usr/share/opensearch/config/opensearch-security/ \
  -icl -nhnv -cacert /usr/share/opensearch/config/root-ca.pem \
  -cert /usr/share/opensearch/config/kirk.pem \
  -key /usr/share/opensearch/config/kirk-key.pem
```
### change opensearch password for exists services
```bash
/usr/share/opensearch/plugins/opensearch-security/tools/securityadmin.sh \
  -cd /usr/share/opensearch/config/opensearch-security/ \
  -icl -nhnv -cacert /usr/share/opensearch/config/root-ca.pem \
  -cert /usr/share/opensearch/config/kirk.pem \
  -key /usr/share/opensearch/config/kirk-key.pem
```
